FROM gcr.io/kaniko-project/executor:debug

# Add the previously built app binary to the image
COPY main  /

ENTRYPOINT ["/main"]
